<?php

class HelloWorld
{
    /**
     * @var PDO
     */
    private $pdo;
	const HELLO = array('fr'=>'Coucou', 'en' => 'Hello');

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function hello($what = 'World', $lang = 'fr')
    {
		
        $sql = "INSERT INTO hello VALUES (" . $this->pdo->quote($what) . ")";
        $this->pdo->query($sql);
        return HELLO[$lang] . ' ' . $what;
    }


    public function what()
    {
        $sql = "SELECT what FROM hello";
        $stmt = $this->pdo->query($sql);
        return $stmt->fetchColumn();
    }
}
